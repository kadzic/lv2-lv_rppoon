using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp18
{
    class Program
    {
        //Main program
        static void PrintDiceNumbers(IList<int> thrownNumbers) //metoda za ispis brojeva kockice
        {
            for(int i = 0; i < 20; i++)
            {
                Console.WriteLine(thrownNumbers[i]);
            }
        }

        static void Main(string[] args)
        {
            DiceRoller roller = new DiceRoller();
            Random random = new Random();

            for(int i = 0; i < 20; i++)
            {
                //roller.InsertDie(new Die(6)); korišten konstruktor za 1. zadatak
                //roller.InsertDie(new Die(6, random)); korišten konstruktor u 2. zadatku
                roller.InsertDie(new Die(6)); //konstruktor za 3. zadatak
            }

            roller.RollAllDice();
            IList<int> thrownNumbers = roller.GetRollingResults();

            PrintDiceNumbers(thrownNumbers);

            DiceRoller roller1 = new DiceRoller();

            roller1.RollAllDice();
            IList<int> thrownNumbers1 = roller1.GetRollingResults();

            for(int i = 0; i < 20; i++)
            {
                if(thrownNumbers1[i] < 0 || i >= 20)
                {
                    throw new ArgumentOutOfRangeException("Error.");
                }
            }

            Logger logger = new Logger("Console", "File");

            logger.Log("Ispis na konzolu!");

            Console.WriteLine("\n");
            PrintDiceNumbers(thrownNumbers1);

            FlexibleDiceRoller roller2 = new FlexibleDiceRoller();

            const int numberOfSides = 6;
            for(int i = 0; i < 20; i++)
            {
                roller2.InsertDie(new Die(numberOfSides));
            }

            for(int i = 0; i < 20; i++)
            {
                roller2.RemoveDiceWithSides(5);
            }

            for(int i = 0; i < 20; i++)
            {
                roller2.RemoveAllDice();
            }

            roller2.RollAllDice();
            
        }
    }
}
