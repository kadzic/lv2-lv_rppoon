using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp18
{
    /* sučelje za 4. zadatak
    interface ILogger
    {
        void Log(string message);
    }
    */

    //sučelje za 5. zadatak
    interface ILogger
    {
        void Log(ILogable data);
    }
}
