using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp18
{
    /* klasa korištena do 5. zadatka
    class DiceRoller 
    {
        private List<Die> dice;
        private List<int> resultForEachRoll;
        //private Logger logger;  Atribut korišten u 3. primjeru sa predloška
        private ILogger logger; // Atribut korišten u 4. zadatku

        /* Konstruktor korišten u 1., 2. i 3. zadatku
        public DiceRoller()
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
        }
        */

        /* Konstruktor iz 3. primjera sa predloška
        public DiceRoller()
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
            this.logger = new Logger("Console", null);
        }
        */

        /*
        //Konstruktor za 4. zadatak
        public DiceRoller()
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
        }

        public void InsertDie(Die die)
        {
            dice.Add(die);
        }

        public void RollAllDice()
        {
            //clear results of previous rolling
            this.resultForEachRoll.Clear();
            foreach(Die die in dice)
            {
                this.resultForEachRoll.Add(die.Roll());
            }
        }

        //View of the results
        public IList<int> GetRollingResults()
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<int>(this.resultForEachRoll);
        }

        public int DiceCount
        {
            get { return dice.Count; }
        }

        //dodana metoda (3. primjer sa predloška)
        public void LogRollingResults()
        {
            foreach(int result in this.resultForEachRoll)
            {
                logger.Log(result.ToString());
            }
        }

        //dodana metoda (za 4. zadatak)
        public void setLogger(ILogger logger)
        {
            this.logger = logger;
        }
    }
    */
    class DiceRoller : ILogable
    {
        private List<Die> dice;
        private List<int> resultForEachRoll;

        public DiceRoller()
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
        }

        public void InsertDie(Die die)
        {
            dice.Add(die);
        }

        public void RollAllDice()
        {
            this.resultForEachRoll.Clear();
            foreach(Die die in dice)
            {
                this.resultForEachRoll.Add(die.Roll());
            }
        }

        public void RemoveAllDice()
        {
            this.dice.Clear();
            this.resultForEachRoll.Clear();
        }

        public IList<int> GetRollingResults()
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<int>(this.resultForEachRoll);
        }

        public int DiceCount
        {
            get { return dice.Count; }
        }

        public string GetStringRepresentation()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach(int result in this.resultForEachRoll)
            {
                stringBuilder.Append(result.ToString()).Append("\n");
            }
            return stringBuilder.ToString();
        }
    }
}
