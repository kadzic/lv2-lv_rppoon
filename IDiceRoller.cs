using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp18
{
    /* sučelje za 5. zadatak
    interface IDiceRoller
    {
        void InsertDie();
        void RemoveAllDice();
        void RollAllDice();
    }
    */

    //sučelja za 6. zadatak
    interface IRoller
    {
        void RollAllDice();
    }

    interface IHandleDice
    {
        void RemoveAllDice();
        void RollAllDice();
    }
}
