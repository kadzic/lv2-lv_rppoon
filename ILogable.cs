using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp18
{
    //sučelje iz 5. primjera za 5. zadatak
    interface ILogable
    {
        string GetStringRepresentation();
    }
}
