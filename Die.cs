using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp18
{
    /* klasa korištena do 7. zadatka
    class Die
    {
        private int numberOfSides;
        //private Random randomGenerator; Atribut korišten u 1. i 2. zadatku
        private RandomGenerator randomGenerator; //3. zadatak

        /* konstruktor za 1. zadatak
        public Die(int numberOfSides)
        {
            this.numberOfSides = numberOfSides;
            this.randomGenerator = new Random();
        }
        */

        /* konstruktor za 2. zadatak
        public Die(int numberOfSides, Random random)
        {
            this.numberOfSides = numberOfSides;
            this.randomGenerator = random;
        }
        */

        /*
        //3. zadatak - konstruktor
        public Die(int numberOfSides)
        {
            this.numberOfSides = numberOfSides;
            this.randomGenerator = RandomGenerator.GetInstance();
        }

        public int Roll()
        {
            //return korišten u 1. i 2. zadatku
            //return randomGenerator.Next(1, numberOfSides + 1); //korišten return zbog toga što nije bio napisan tip podatka varijable rolledNumber

            return this.randomGenerator.NextInt(1, numberOfSides + 1); //3. zadatak
        }
    }
    */

    //klasa za 7. zadatak
    class Die
    {
        private int numberOfSides;
        private RandomGenerator randomGenerator;

        public Die(int numberOfSides)
        {
            this.numberOfSides = numberOfSides;
            this.randomGenerator = RandomGenerator.GetInstance();
        }

        public int Roll()
        {
            return this.randomGenerator.NextInt(1, numberOfSides + 1);
        }

        public int NumberOfSides
        {
            get { return this.numberOfSides; }
        }
    }
}
